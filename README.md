# Bitbucket Pipelines Pipe: Dynatrace Push Event

This pipe show how easy is to [Push Dynatrace Events](https://www.dynatrace.com/support/help/extend-dynatrace/dynatrace-api/environment-api/events/post-event/) from your pipelines.  Use cases:

1. After running a performance test, push a ```CUSTOM_ANNOTATION``` event with the test start/stop times, description and type
1. After a deployment, push a ```CUSTOM_DEPLOYMENT``` event with a description
1. After a configuration change, push a ```CUSTOM_CONFIGURATION``` event with a description and configuration details
1. For other notable events, push a ```CUSTOM_INFO``` event with a title and description

Read more about this pipe in the [Atlassian Marketplace](https://marketplace.atlassian.com/apps/1223039/dynatrace-pipes-for-bitbucket?hosting=cloud&tab=overview)

See the pipe listing on the [Atlassian integrations directory](https://bitbucket.org/product/features/pipelines/integrations?&search=dynatrace)

See an example pipeline using this pipe in [this bitbucket repo](https://bitbucket.org/dynatracedemos/simplenodeservice)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file.  See table below for other required fields that vary by event type.

```yaml
- pipe: dynatrace/dynatrace-push-event:0.1.0
  variables:
    DYNATRACE_BASE_URL: "<string>"
    DYNATRACE_API_TOKEN: "<string>"
    EVENT_TYPE: "<string>"
    TAG_RULE: "<string>"
    DEBUG: "<string>"
```

## Variables

These pipe variables map to the Dynatrace required and optional variables for the [API](https://www.dynatrace.com/support/help/shortlink/api-events-post-event#parameters-mapping-) 

### All events

| Variable | Usage |
| --- | --- |
| DYNATRACE_BASE_URL (*)  | Dynatrace Base URL |
| DYNATRACE_API_TOKEN (*) | Dynatrace API Token |
| EVENT_TYPE (*)          | CUSTOM_DEPLOYMENT, CUSTOM_INFO, CUSTOM_CONFIGURATION or CUSTOM_ANNOTATION  |
| TAG_RULE (*)            | value for [TagMatchRule](https://www.dynatrace.com/support/help/shortlink/api-events-post-event#events-post-parameter-tagmatchrule). Double quotes must be escaped x`|
| SOURCE                  | Optional Description for Source. Default: `BitbucketPipeline` |
| START                   | Optional Start time of event. Times must be in ISO UTC with NO offsets |
| END                     | Optional End time of event. Times must be in ISO UTC with NO offsets |
| DEBUG                   | Optional Turn on extra debug information. Default: `false`. |
| CUSTOM_PROPERTIES       | Optional Collection of key and value pairs |

_(*) = required variable._

### Additional Variables for CUSTOM_DEPLOYMENT

| Variable | Usage |
| --- | --- |
| DEPLOYMENT_NAME (*)    | Description for deployment name |
| DEPLOYMENT_VERSION (*) | Description for deployment version. Default: $BITBUCKET_BUILD_NUMBER |
| DEPLOYMENT_PROJECT     | Description for deployment project. Default: $BITBUCKET_PROJECT_KEY |
| DEPLOYMENT_REMEDIATION_ACTION | Optional Remediation value.  Can be a URL value |

_(*) = required variable._

### Additional Variables for CUSTOM_CONFIGURATION

| Variable | Usage |
| --- | --- |
| DESCRIPTION (*)        | General Description |
| CONFIGURATION (*)      | Description for configuration change |
| ORIGINAL_CONFIGURATION | Optional Description for original configuration |

_(*) = required variable._

### Additional Variables for CUSTOM_ANNOTATION

| Variable | Usage |
| --- | --- |
| ANNOTATION_TYPE (*)        | Description for annotation change |
| ANNOTATION_DESCRIPTION (*) | Detailed Description for annotation change |
| DESCRIPTION                | Optional General Description |

_(*) = required variable._

### Additional Variables for CUSTOM_INFO

| Variable | Usage |
| --- | --- |
| TITLE | Title for custom info event, Optional but recommended |

_(*) = required variable._

## Prerequisites

1. Dynatrace tenant with an [API token](https://www.dynatrace.com/support/help/extend-dynatrace/dynatrace-api/basics/dynatrace-api-authentication/) that has write configuration permissions
1. Some monitored entity that can be identified with a Dynatrace [TagMatchRule](https://www.dynatrace.com/support/help/shortlink/api-events-post-event#events-post-parameter-tagmatchrule)

## Examples

It is recommended to use [Bitbucket respository variables](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/) for `DYNATRACE_BASE_URL` and `DYNATRACE_API_TOKEN`.  The expected format for `DYNATRACE_BASE_URL` is something like `https://[tenant].live.dynatrace.com`

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

Deployment Event

```yaml
- pipe: dynatrace/dynatrace-push-event:0.1.0
  variables:
    DYNATRACE_BASE_URL: $DYNATRACE_BASE_URL
    DYNATRACE_API_TOKEN: $DYNATRACE_API_TOKEN
    EVENT_TYPE: "CUSTOM_DEPLOYMENT"
    SOURCE: "Helm Charts"
    DEPLOYMENT_NAME: "QA Environment"
    DEPLOYMENT_VERSION: $VERSION
    TAG_RULE: "[{\"meTypes\":\"SERVICE\",\"tags\":[{\"context\":\"ENVIRONMENT\",\"key\":\"service\",\"value\":\"customer\"}]}]"
    DEBUG: "true"
```

Annotation Event for a Performance Test using CUSTOM_PROPERTIES for additional information

```yaml
- START="$(date -u --date "+1 min" +"%Y-%m-%dT%H:%M:%SZ")"
- END="$(date -u --date "+10 min" +"%Y-%m-%dT%H:%M:%SZ")"
- pipe: dynatrace/dynatrace-push-event:0.1.0
  variables:
    DYNATRACE_BASE_URL: $DYNATRACE_BASE_URL
    DYNATRACE_API_TOKEN: $DYNATRACE_API_TOKEN
    EVENT_TYPE: "CUSTOM_ANNOTATION"
    DESCRIPTION: "Performance Test"
    START: $START
    END: $END
    TAG_RULE: "[{\"meTypes\":\"SERVICE\",\"tags\":[{\"context\":\"ENVIRONMENT\",\"key\":\"service\",\"value\":\"customer\"}]}]"
    DEBUG: "true"
    CUSTOM_PROPERTIES: "{\"Test Scenario\":\"Stress Test\", \"Load Script\":\"My Test Script\"}"
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know. The pipe is maintained by Dynatrace. You can contact us directly at integrations@dynatrace.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
