#!/usr/bin/env bats

setup() {
  DEBUG=true
  DOCKER_IMAGE="test/dynatrace-push-event"
  docker build -t ${DOCKER_IMAGE} .
}

@test "push CUSTOM_DEPLOYMENT event" {
  #skip
  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN=$TEST_DYNATRACE_API_TOKEN \
      -e EVENT_TYPE=CUSTOM_DEPLOYMENT \
      -e SOURCE="bats testing" \
      -e DEPLOYMENT_NAME="bats testing DEPLOYMENT_NAME" \
      -e DEPLOYMENT_VERSION="bats testing DEPLOYMENT_VERSION" \
      -e DEPLOYMENT_REMEDIATION_ACTION="http://DEPLOYMENT_REMEDIATION_ACTION" \
      -e TAG_RULE=$TEST_TAG_RULE \
      -e BITBUCKET_BUILD_NUMBER="bats testing BITBUCKET_BUILD_NUMBER" \
      -e BITBUCKET_DEPLOYMENT_ENVIRONMENT="bats testing BITBUCKET_DEPLOYMENT_ENVIRONMENT" \
      -e BITBUCKET_GIT_HTTP_ORIGIN="bats testing BITBUCKET_GIT_HTTP_ORIGIN" \
      -e BITBUCKET_COMMIT="bats testing BITBUCKET_COMMIT" \
      -e BITBUCKET_BRANCH="bats testing BITBUCKET_BRANCH" \
      -e BITBUCKET_TAG="bats testing BITBUCKET_TAG" \
      -e BITBUCKET_PR_DESTINATION_BRANCH="bats testing BITBUCKET_PR_DESTINATION_BRANCH" \
      -e BITBUCKET_PROJECT_KEY="bats testing BITBUCKET_PROJECT_KEY" \
      -e BITBUCKET_REPO_FULL_NAME="bats testing BITBUCKET_REPO_FULL_NAME" \
      -e BITBUCKET_REPO_OWNER="bats testing BITBUCKET_REPO_OWNER" \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "push CUSTOM_CONFIGURATION event" {
  #skip

  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN=$TEST_DYNATRACE_API_TOKEN \
      -e EVENT_TYPE=CUSTOM_CONFIGURATION \
      -e SOURCE="bats testing" \
      -e DESCRIPTION="bats testing DESCRIPTION" \
      -e CONFIGURATION="bats CONFIGURATION" \
      -e ORIGINAL_CONFIGURATION="bats ORIGINAL_CONFIGURATION" \
      -e TAG_RULE=$TEST_TAG_RULE \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "push CUSTOM_ANNOTATION event" {
  #skip

  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN=$TEST_DYNATRACE_API_TOKEN \
      -e EVENT_TYPE=CUSTOM_ANNOTATION \
      -e SOURCE="bats testing" \
      -e DESCRIPTION="bats testing DESCRIPTION" \
      -e ANNOTATION_DESCRIPTION="bats testing ANNOTATION_DESCRIPTION" \
      -e ANNOTATION_TYPE="bats testing ANNOTATION_TYPE" \
      -e TAG_RULE=$TEST_TAG_RULE \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "push CUSTOM_ANNOTATION event with start end" {
  #skip

  # current time and ten minutes ago
  START="$(date -u --date "-10 min" +"%Y-%m-%dT%H:%M:%SZ")"
  END="$(date -u "+%Y-%m-%dT%H:%M:%SZ")"

  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN=$TEST_DYNATRACE_API_TOKEN \
      -e EVENT_TYPE=CUSTOM_ANNOTATION \
      -e SOURCE="bats testing" \
      -e DESCRIPTION="bats testing DESCRIPTION" \
      -e ANNOTATION_DESCRIPTION="bats testing ANNOTATION_DESCRIPTION start end" \
      -e ANNOTATION_TYPE="bats testing ANNOTATION_TYPE" \
      -e START="$START" \
      -e END="$END" \
      -e TAG_RULE=$TEST_TAG_RULE \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "push CUSTOM_INFO event" {
  #skip

  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN=$TEST_DYNATRACE_API_TOKEN \
      -e EVENT_TYPE=CUSTOM_INFO \
      -e TITLE="bats testing CUSTOM_INFO" \
      -e SOURCE="bats testing" \
      -e DESCRIPTION="bats testing DESCRIPTION" \
      -e TAG_RULE=$TEST_TAG_RULE \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "push CUSTOM_DEPLOYMENT event with bad DT Creds" {
  #skip

  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN="BAD_TOKEN" \
      -e EVENT_TYPE=CUSTOM_DEPLOYMENT \
      -e SOURCE="bats testing" \
      -e DEPLOYMENT_NAME="bats testing DEPLOYMENT_NAME" \
      -e DEPLOYMENT_VERSION="bats testing DEPLOYMENT_VERSION" \
      -e TAG_RULE=$TEST_TAG_RULE \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "push CUSTOM_DEPLOYMENT event with missing argumment" {
  #skip

  run docker run \
      -e DEBUG=$DEBUG \
      -e DYNATRACE_BASE_URL=$TEST_DYNATRACE_BASE_URL \
      -e DYNATRACE_API_TOKEN=$TEST_DYNATRACE_API_TOKEN \
      -e EVENT_TYPE=CUSTOM_DEPLOYMENT \
      -e SOURCE="bats testing" \
      -e TAG_RULE=$TEST_TAG_RULE \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}