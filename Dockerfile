FROM alpine:3.10.3

LABEL description="Send an event to Dynatrace"
LABEL repository="https://bitbucket.org/dynatrace/dynatrace-push-event"

RUN apk --no-cache add \
    bash

RUN apk add --update coreutils && rm -rf /var/cache/apk/*

RUN apk add curl

ADD https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh /
COPY pipe /
COPY LICENSE README.md pipe.yml /

WORKDIR /opt/atlassian/bitbucketci/agent/build
ENTRYPOINT ["/pipe.sh"]
