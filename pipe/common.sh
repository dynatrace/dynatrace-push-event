# this file is to support local testing of the pipe.sh
# when run in docker, the common.sh is brought in for bitbucket execution
info() {
  echo $1
}
debug() {
  echo $1
}
success() {
  echo $1
}
fail() {
  echo $1
}
enable_debug() {
  echo ""
}