# Contributing to Bitbucket Pipes

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

# Release process

The ```bitbucket-pipelines.yml``` BitBucket pipeline automates the process build and push the pipe to a registry.  

Image versions are increased automatically using semantic versioning using a tool called [`semversioner`](https://pypi.org/project/semversioner/). 

Once pushed, the pipeline will call `ci-scripts/release.sh`.  This script will:

- run the test suite defined in the ```test/``` subfolder
- Generate new image version number based on the changeset types `major`, `minor`, `patch`
- build and tag the docker image
- push the docker image to the defined registry

If there are change sets, the pipeline will:

- Generate a new file in `.changes` directory with all the changes for this specific version
- (Re)generate the CHANGELOG.md file
- Bump the version number in `README.md` example and `pipe.yml` metadata
- Commit and push changes back to the repository
- Tag your commit with the new semantic version number

## BitBucket Prerequisites

* Define REGISTRY_PASSWORD and REGISTRY_USERNAME repository variables for the registry
* Define DT_URL, DT_TOKEN, TAG_RULE repository variables to support tests

# Development Process

## 1. Prerequisites

* Python & pip
* semversioner (tested with 0.1.4) -- ```pip install semversioner```
* Mac or Unix environment. For the current helper script are written as UNIX shell scripts
* Dynatrace tenant with an API token
* Some monitored entity that can be identified with a Dynatrace TagMatchRule

## 2. Process to develop changes

1. Clone this repo
1. Make a new branch with the prefix of ```feature-```
1. If have your own container registry, then adjust repo as required
1. Make changes to code
1. Test locally following instructions below

## 3. Local testing Testing

To run tests locally you need to:

1. Install [bats](https://opensource.com/article/19/2/testing-bash-bats) (bash test runner) using apt or any suitable package manager

    ```
    apt-get install bats
    ```

1. Make sure you've set up all required environment variables required for testing. Usually, these are the same variables that are required for a pipe to run.

1. Setup ENV variables that are unique to your setup and are secrets we dont want to check in.

    ```
    export TEST_DYNATRACE_BASE_URL=https://YOUR-TENANT.live.dynatrace.com
    export TEST_DYNATRACE_API_TOKEN=YOUR-API-TOKEN
    export TEST_TAG_RULE="[{\"meTypes\":\"SERVICE\",\"tags\":[{\"context\":\"ENVIRONMENT\",\"key\":\"service\",\"value\":\"customer\"}]}]"
    ```

1. Run bats from the root project folder
    ```
    bats test/test*
    ```

## 4. Testing from bitbucket pipeline

You will need to:

1. uncomment the test step in `bitbucket-pipelines.yml`
1. Define the following repository variables to support the tests in `test/test.bat`

    * TEST_DYNATRACE_BASE_URL
    * TEST_DYNATRACE_API_TOKEN
    * TEST_TAG_RULE

## 5. Push the feature change to bitbucket 

1. Push changes to bitbucket and the pipeline will run.  This will build the image and push it to the registry
1. Test the newly create pipe in another pipeline to ensure it works

## 6. Bump the version

1. Once tested, call the ```pushchange.sh``` script that will call the semantic versioning script and git to push the changes back to bitbucket.  This in turn will run the pipeline
1. Once the BitBucket pipeline runs, then run ```git pull``` to bring back the updates change logs locally
1. Once finalized, make a Pull Request in BitBucket to merge the feature branch to master
1. Test the newly create pipe in another pipeline to ensure it works
1. Delete feature branch
